package sh.main;

import java.sql.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import sh.daos.CustomerDao;
import sh.pojo.Customer;
import sh.util.Hb3Util;

public class Hb1Main {
	public static void main(String[] args) {
		CustomerDao custDao = new CustomerDao();
		
		Customer cust = custDao.findCustomerById(2);
		System.out.println(cust);
		
		List<Customer> list = custDao.findAll();
		list.forEach(System.out::println);
		
		Customer newCust = new Customer(20, "Vardhman", "vardhman", "8482923285", "Pune", "vardhmanp@cdac.in", new Date(2000,1,1));
		custDao.addCustomer(newCust);
	}
	
	public static void main1(String[] args) {
		
		// Get Session Factory Object
		SessionFactory factory = Hb3Util.getSessionFactory();
		
		// Create Session Object from Session Factory Object
		Session session = factory.openSession();
		
		// Get customer having primary key 2
		Customer customer = session.get(Customer.class, 2);
		/*
		 * Behind the scene hibernate use prepared statement to create the SQL query and the fire it on database
		 * It create Customer object using reflection technique and by default it calls parameter less Constructor of Customer class.
		 * 	This is the reason we must have to provide parameter less constructor for entity class implicitly or explicitly.  
		 * Then hibernate populate customer class with data obtained from database by using setter methods and return it. 
		 */
		
		/*
		 * So above particular line is replacement of below JDBC code 
		 *
		public Customer findCustomer(String email) throws Exception {
			String sql = "SELECT id, name, password, mobile, address, email, birth FROM customers WHERE email=?";
			try(PreparedStatement stmt = con.prepareStatement(sql)) {
				stmt.setString(1, email);
				ResultSet rs = stmt.executeQuery();
				if(rs.next()) {
					Customer c = new Customer();
					c.setId( rs.getInt("id") );
					c.setName( rs.getString("name") );
					c.setPassword( rs.getString("password") );
					c.setMobile( rs.getString("mobile") );
					c.setAddress( rs.getString("address") );
					c.setEmail( rs.getString("email") );
					c.setBirth( rs.getDate("birth") );
					return c;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		*/
		if (customer == null)
			System.out.println("Customer Not Found");
		else
			System.out.println(customer);
		
		Query<Customer> q = session.createQuery("from Customer");
		List<Customer> list = q.getResultList();
		list.forEach(System.out::println);
		
		
		// Close Session and Session Factory
		session.close();
		Hb3Util.shutdown();
	}
}
