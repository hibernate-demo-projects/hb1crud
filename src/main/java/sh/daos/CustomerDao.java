package sh.daos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import sh.pojo.Customer;
import sh.util.Hb3Util;

public class CustomerDao {

	/*
	 * Create Session object From Session Factory for every new Query.
	 */

	/*
	 * Find Customer By Id
	 */
	public Customer findCustomerById(int id) {
		Customer cust = null;

		// Get Session Factory Object and Create new Session Object from it
		// Use try with resource to ensure Session Object will close automatically
		try (Session session = Hb3Util.getSessionFactory().openSession()) {
			cust = session.get(Customer.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cust;
	}

	/*
	 * Find All Customers
	 */
	public List<Customer> findAll() {
		List<Customer> list = null;

		try (Session session = Hb3Util.getSessionFactory().openSession()) {

			// Create Query Object from Session Object
			Query<Customer> query = session.createQuery("from Customer");
			list = query.getResultList();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void addCustomer(Customer cust) {
		Transaction tx = null;
		try (Session session = Hb3Util.getSessionFactory().openSession()) {
			tx = session.beginTransaction();
			Serializable primaryKey = session.save(cust);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
	}
}