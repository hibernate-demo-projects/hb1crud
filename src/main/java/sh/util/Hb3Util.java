package sh.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

// Hibernate 3 Bootstraping (work with hibernate 4.x and 5.x also)
public class Hb3Util {
	private static final SessionFactory sessionFactory = buildSessionFactory();
	
	private static SessionFactory buildSessionFactory() {
		
		// Create hibernate config object
		Configuration cfgObject = new Configuration();
		
		// Read configuration from hibernate.cfg.xml file
		cfgObject.configure();
		
		/*
		 * If our hibernate configuration file have name other than `hibernate.cfg.xml` this 
		 * then provide that name in configure("<hibernate_config_file_name>.xml"); method.
		 */
		
		// Build and return Session Factory Object
		return cfgObject.buildSessionFactory();
	}
	
	public static SessionFactory getSessionFactory() {
		return Hb3Util.sessionFactory;
	}
	
	public static void shutdown() {
		Hb3Util.sessionFactory.close();
	}

}
