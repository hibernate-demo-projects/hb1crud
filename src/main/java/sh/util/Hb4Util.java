package sh.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import sh.pojo.Customer;

// Hibernate 4 Bootstraping
public class Hb4Util {
	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry registry;

	private static SessionFactory buildSessionFactory() {
		
		// Create Service Registry
		registry = new StandardServiceRegistryBuilder()
				.configure()	// This method reads `hibernte.cfg.xml` file
				.build();		// This method returns registry object
		
		/*
		 * If we don't to read hibernate properties from `hibernate.cfg.xml` and want to enter data hard code then we can do this as below
		 *
		registry = new StandardServiceRegistryBuilder()
				.applySetting("hibernate.connection.driver", "com.mysql.cj.jdbc.Driver")
				.applySetting("hibernate.connection.username","shivamp")
				.applySetting("hibernate.connection.password","shivamp")
				.applySetting("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect")
				.applySetting("hibernate.show_sql","true")
				.build();
		 */
		
		// Create Meta Data
		Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
		
		/*
		 * If we don't want to read mapping information from `hibernate.cfg.xml` file and want to enter data hard code then we cando this as below
		 *
		Metadata meta = new MetadataSources(registry).addAnnotatedClass(Customer.class).buildMetadata();
		 *
		 */

		// Create Session Factory Object
		SessionFactory factory = metadata.getSessionFactoryBuilder().build();
		
		return factory;
	}

	public static SessionFactory getSessionFactory() {
		return Hb4Util.sessionFactory;
	}

	public static void shutdown() {
		Hb4Util.sessionFactory.close();
	}

}
